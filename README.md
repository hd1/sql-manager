### What is this repository for? ###

SQL-Manager manages your RDBMS for you.

### How do I get set up? ###

* Summary of set up

mvn package &&  target/sql-manager-1.0-SNAPSHOT.jar

* Dependencies

1. You'll need [maven](http://maven.apache.org) to build.
2. You'll need your database JDBC driver on your classpath.

* Database configuration

You'll need to allow access from your host to the database server. Standard JDBC caveats apply.

### Contribution guidelines ###

If you have an idea for additional functionality, put it as an issue and I'll get to it.

### Who do I talk to? ###

* Repo owner or admin

Hasan Diwan <hasan.diwan@gmail.com> http://prolificprogrammer.com