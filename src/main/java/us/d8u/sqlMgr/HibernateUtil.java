package us.d8u.sqlMgr;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.cfg.Configuration;
import org.reflections.Reflections;

public class HibernateUtil {
    private static SessionFactory sessionFactory;
    private static Logger logger = Logger.getLogger("us.d8u.sqlMgr.HibernateUtil");

    static{
        AnnotationConfiguration cfg;
        if (System.getProperty("connection.password") == null) {
            System.setProperty("connection.password", "");
        }
        try {
            cfg = new AnnotationConfiguration()
            .setProperty("hibernate.dialect", System.getProperty("dialect")) 
            .setProperty("hibernate.connection.url", System.getProperty("connection.url"))
            .setProperty("hibernate.conncetion.username", System.getProperty("connection.username"))
            .setProperty("hibernate.connection.password", System.getProperty("connection.password"))
            .setProperty("hibernate.use_sql_comments", "true")
            .addPackage(System.getProperty("models.pakcage"));
            sessionFactory = cfg.buildSessionFactory();
        } catch (NullPointerException npe) {
            logger.fatal("The following properties must be defined:\ndialect\tJPA dialect one wishes to use\nconnection.url\tJDBC URL\nconnection.username\tYour Username\nconnection.password\tYour password\nmodels.package\tPackage containing your models on the classpath\n");
            System.exit(-1);
        } catch (Throwable e) {
            throw new ExceptionInInitializerError(e);
        }
    }

    public static SessionFactory getSessionFactory(){
        return sessionFactory;
    }

    public static void shutdown(){
        //closes caches and connections
        getSessionFactory().close();
    }

    public static void main(String[] args) {
       getSessionFactory();
       shutdown();
    }
}
