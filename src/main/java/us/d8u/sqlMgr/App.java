package us.d8u.sqlMgr;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        HibernateUtil.getSessionFactory().openSession();
        HibernateUtil.shutdown();
    }
}
